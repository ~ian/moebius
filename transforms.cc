/*
 * Transformations
 */

#include <math.h>
#include <stdio.h>

#include "transforms.hh"

RotationTransform::RotationTransform(double theta) {
  s= sin(theta); c= cos(theta);
}

Point XZRotationTransform::operator()(Point in) {
  return Point(in[0]*c + in[2]*s,
               in[1],
               in[2]*c - in[0]*s);
}

Point YZRotationTransform::operator()(Point in) {
  return Point(in[0],
               in[1]*c + in[2]*s,
               in[2]*c - in[1]*s);
}

TearOpenTransform::TearOpenTransform(double amt, double apy) {
  factor= pow(10.0,amt);
  apexy= apy;
}

Point TearOpenTransform::operator()(Point in) {
  double w= apexy - in[1];
  double r= sqrt(in[2]*in[2] + w*w);
  double theta= r>0.0 ? factor*atan2(in[2],w) : 0.0;
  double ny= apexy - r*cos(theta);
  double nz= r*sin(theta);
  return Point(in[0],ny,nz);
}

BulgeTransform::BulgeTransform(double amt, double stddev, double apy) {
  amount=amt; apexy=apy;
  variance= stddev*stddev;
}

Point BulgeTransform::operator()(Point in) {
  double w= in[1] - apexy;
  double r2= w*w + in[2]*in[2];
  double nror= 1.0 + amount*exp(-r2/variance);
  double ny= w*nror + apexy;
  return Point(in[0],ny,in[2]);
}

Point ShearTransform::operator()(Point in) {
  double dx= (in[0] - epicentre[0]) / deviation[0];
  double dy= (in[1] - epicentre[1]) / deviation[1];
  double dz= (in[2] - epicentre[2]) / deviation[2];
  double r2= dx*dx + dy*dy + dz*dz;
  double amount= exp(-r2);
  return in + magnitude*amount;
}

Point TwistTransform::operator()(Point in) {
  double w= in[1] - apexy;
  double theta= factor*in[2];
  double c= cos(theta), s= sin(theta);
  double nx= in[0]*c - w*s;
  double ny= w*c + in[0]*s + apexy;
  return Point(nx,ny,in[2]);
}

Point ScaleTransform::operator()(Point in) {
  return in*scale;
}

Point MagicTransform::operator()(Point in) {
  double nz= in[2]*zfactor;
  double r= sqrt(in[0]*in[0] + in[1]*in[1]);
  double nx= xfactor * (r - xkonstant);
  double nr= yoffset + yfactor*in[1];
  double ny2=  nr*nr- nz*nz;
//fprintf(stderr,"M %7.4f,%7.4f,%7.4f r=%7.4f nx=%7.4f nz=%7.4f nr=%7.4f ny2=%7.4f\n",
//        in[0],in[1],in[2], r,nx,nz,nr,ny2);
  double ny= sqrt(ny2);
  return Point(nx,ny,nz);
}
