/*
 * Points library
 */

#include <stdlib.h>
#include <stdio.h>

#include "library.hh"
#include "transforms.hh"

double Point::planedistance= 1.0;
double Point::eyedistance= 1.0;
double Point::cutoffdistance= 10.0;
double Point::eyex= 0.0;

TransformList Point::usertransforms;
TransformList Point::povtransform;

Point TransformList::operator()(Point it) {
  for (int i=0; i<used; i++) { it= (*a[i])(it); }
  return it;
}

void TransformList::append(Transform *it) {
  if (used >= size) {
    size+=5; size<<=1;
    a= (Transform**)realloc(a,sizeof(Transform*)*size);
    if (!a) { perror("realloc"); exit(1); }
  }
  a[used++]= it;
}

void TransformList::clearcontents() {
  for (int i=0; i<used; i++) delete a[i];
}
  
Point::operator Onscreen() const {
  Point it= transformed();
  double factor= eyedistance / (eyedistance + planedistance - it[2]);
  return Onscreen(it[0] * factor + eyex * (1.0-factor), it[1] * factor);
}

void Point::setobserver(double theta, double eta, double planedist, double eyedist,
                        double cutoffdist) {
  planedistance= planedist;
  eyedistance= eyedist;
  cutoffdistance= cutoffdist;
  povtransform.reset();
  if (theta != 0.0) povtransform.append(new XZRotationTransform(theta));
  if (eta != 0.0) povtransform.append(new YZRotationTransform(eta));
}
