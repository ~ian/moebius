/*
 * Output functions
 */

#ifndef OUTPUT_HH
#define OUTPUT_HH

#include "library.hh"

struct Output {
  virtual ~Output(){};
  enum Colour { grid, solidblack, solidwhite };
  virtual void drawcell(const Point *list, int n, Colour colour) =0;
  virtual void startimage(){};
  virtual void endimage(){};
};

#endif
