/*
 * X11 functions - colour-stereographic output
 * (currently broken)
 */

#ifndef DUALX11_HH
#define DUALX11_HH

#include <stdlib.h>
#include <X11/Xlib.h>

#include "output.hh"

struct DualX11Output : Output {
  DualX11Output();
  ~DualX11Output();
  void drawcell(const Point *list, int n, Colour colour);
  void startimage();
  void endimage();
private:
  Display *display;
  Window window;
  GC fabric[2];
  GC mesh[2];
  unsigned long planemasks[2], pixelbase;
};

#endif
