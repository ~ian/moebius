/*
 * X11 functions
 */

#ifndef X11_HH
#define X11_HH

#include <stdlib.h>
#include <X11/Xlib.h>

#include "output.hh"

struct X11Output : Output {
  X11Output();
  ~X11Output();
  void drawcell(const Point*, int, Colour);
  void startimage();
  void endimage();
private:
  Display *display;
  Window window;
  Colormap cmap;
  GC black, white, red, blue;
  GC gc(const char *colour_name);
};

#endif
