/*
 * PostScript output
 */

#include <iostream.h>
#include "postscript.hh"

PostScriptOutput::PostScriptOutput(ofstream& f)
: file(f) {

  file <<
    "%!PS-Adobe-2.0 EPSF-2.0\n"
    "%%Creator: moebius\n"
    "%%BoundingBox: -500 -500 500 500\n"
    "%%Pages: 1\n"
    "%%DocumentFonts: \n"
    "%%EndComments\n"
    "    1 setlinecap 1 setlinejoin 0.002 setlinewidth\n"
    "    500 500 scale\n"
    "%%EndProlog\n"
    "%%Page: 1 0\n"
    "    save\n";
}

PostScriptOutput::~PostScriptOutput() {
  file <<
    "    restore\n"
    "%%Trailer\n"
    "%%EOF\n";
}

void PostScriptOutput::drawcell(const Point* list, int n, Colour colour) {
  Onscreen p[n];
  for (int i=0; i<n; i++) p[i]= Onscreen(list[i]);
  switch (colour) {
  case grid:
    docell(p,n,"1 setgray fill");
    docell(p,n,"0 setgray stroke");
    break;
  case solidwhite:
    docell(p,n,"1 setgray fill");
    break;
  case solidblack:
    docell(p,n,"0 setgray fill");
    break;
  default:
    abort();
  }
}

void PostScriptOutput::docell(const Onscreen* list, int n, const char *what) {
  file << "    newpath\n";
  file << "        " << list->x << " " << list->y << " moveto\n";
  list++;
  for (int i=1; i<n; i++, list++) {
    file << "        " << list->x << " " << list->y << " lineto\n";
  }
  file << "      closepath  " << what << "\n";
}
