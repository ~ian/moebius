CPPFLAGS= -I/usr/include \
	-Wall -O2 -g -Wmissing-prototypes -Wstrict-prototypes \
	-Wno-deprecated \
	$(WERROR)
WERROR=-Werror
LDFLAGS= 
LIBS= 

OBJS=	dualx11.o x11.o main.o parameter.o graphics.o library.o \
	transforms.o postscript.o moebius.o

a.out:	$(OBJS)
	$(LINK.cc) -o a.out $(OBJS) -lm -L/usr/X11R6/lib -lX11

depend:
	$(C++) $(CPPFLAGS) -E -MM *.cc >.depend

clean:
	rm -f $(OBJS) a.out

include .depend
