/*
 * X11 functions
 */


#include "x11.hh"
#include "parameter.hh"

static Parameter<int> x11size("x11size", "X11 window size", 500, 100, 10, 10000);

GC X11Output::gc(const char *colour_name) {
  XGCValues gcvalues;
  XColor colour;
  Status st;

  st= XAllocNamedColor(display,cmap,colour_name,&colour,&colour);
  if (!st) {
    cerr << "cannot allocate colour " << colour_name << ", quitting\n";
    exit(1);
  }

  gcvalues.foreground= gcvalues.background= colour.pixel;
  return XCreateGC(display,window,GCForeground|GCBackground,&gcvalues);
}

X11Output::X11Output() {
  display= XOpenDisplay(0);
  window= XCreateSimpleWindow(display,
                              DefaultRootWindow(display),
                              0,0, x11size,x11size, 0,0,0);
  cmap= DefaultColormap(display,DefaultScreen(display));

  black= gc("black");
  white= gc("white");
  blue= gc("blue");
  red= gc("red");

  XSelectInput(display,window,0);
  XMapWindow(display,window);
  XFlush(display);
}

void X11Output::startimage() {
  XClearWindow(display,window);
}

void X11Output::endimage() {
  XFlush(display);
}

X11Output::~X11Output() {
  XCloseDisplay(display);
}

void X11Output::drawcell(const Point* list, int n, Colour colour) {
  GC fill, draw;
  
  XPoint xp[n+1];
  for (int i=0; i<n; i++) {
    Onscreen here= Onscreen(list[i]);
    xp[i].x= (int)((here.x+1.0)*(x11size*0.5));
    xp[i].y= (int)((-here.y+1.0)*(x11size*0.5));
  }
  switch (colour) {
  case grid:       fill= black;  draw= white;  break;
  case solidblack: fill= red;    draw= 0;      break;
  case solidwhite: fill= blue;   draw= 0;      break;
  default: abort();
  }
  XFillPolygon(display,window,fill,xp,n,Nonconvex,CoordModeOrigin);
  if (draw) {
    xp[n]= xp[0];
    XDrawLines(display,window,draw,xp,n+1,CoordModeOrigin);
  }
}
