/*
 * Moebius strip
 */

#ifndef MOEBIUS_HH
#define MOEBIUS_HH

class Surface {
public:
  // t and u vary from 0 to 1.0
  virtual Point middlepoint(double t, double u) =0;
};

class Edge {
public:
  // t varies from 0 to 1.0
  virtual Point edgepoint(double t) =0;
};

class Moebius : public Surface, Edge {
public:
  virtual Point edgepoint(double t) =0;
  virtual Point middlepoint(double t, double u) =0;
};

class MoebiusStrip : public Moebius {
  // Strip is `lying' in the XY plane. The crossing is at the
  // maximal Y value, where the edge which has X*Z positive
  // has smaller Y at the point where X=Z=0 than the `other' edge,
  // which has X*Z negative.  The flat part of the strip
  // is at minimal Y value.  Here one edge has Z positive and
  // the other Z negative.
  // X and Y range from -1.0 to 1.0;
  // Z ranges from -halfbreadth to +halfbreadth.
  double halfgap;     // 1/2 the width of the strip at the crossing point
  double halfbreadth; // 1/2 the width of the strip at the flat part
public:
  MoebiusStrip() { halfgap= 0.15; halfbreadth= 0.15; }
  MoebiusStrip(double b) { halfgap= halfbreadth= b/2.0; }
  MoebiusStrip(double hg, double hb) { halfgap= hg/2.0; halfbreadth= hb/2.0; }
    
  Point edgepoint(double t);
  Point middlepoint(double t, double u);
};

class MoebiusEnfoldmentAny : public Moebius {
public:
  Point edgepoint(double t);
};

class MoebiusEnfoldment : public MoebiusEnfoldmentAny {
  double thickness;
  double bottomportion;
public:
  MoebiusEnfoldment(double t=.35, double bp=.5) { thickness= t; bottomportion= bp; }
  Point middlepoint(double t, double u);
};

class MoebiusEnfoldmentNew : public MoebiusEnfoldmentAny {
public:
  MoebiusEnfoldmentNew() { }
  Point middlepoint(double t, double u);
};

#endif
