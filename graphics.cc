/*
 * Graphics library
 */

#include <iostream.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "library.hh"
#include "graphics.hh"
#include "output.hh"
#include "parameter.hh"

static int checkneededline(Onscreen a, Onscreen b) {
  double xd= a.x - b.x;
  double yd= a.y - b.y;
  int n= (int)ceil(sqrt(xd*xd+yd*yd)*100.0);
  return n>0 ? n : 1;
}

static Parameter<double>
sotextpit("sotextpit", "Pitch of solid texture (0: grid mode)",
	  0., 1., 0., 1000.);

void Cell::display(Output& o) {
  int nn[4];
  int totalnn= 0;
  int i;
  for (i=0; i<4; i++) {
    nn[i]= checkneededline(p[i],p[(i+1)&3]);
    totalnn+= nn[i];
  }
  Point *array= new Point[totalnn];
  Point *inarrayp= array;
  Point mean(0,0,0);
  for (i=0; i<4; i++) {
    for (int a=0; a<nn[i]; a++) {
      double fp= (double)a         / nn[i];
      double fn= (double)(nn[i]-a) / nn[i];
      *inarrayp++= p[i]*fn + p[(i+1)&3]*fp;
    }
    mean = mean + p[i];
  }
  mean = mean * 0.25;
  Output::Colour colour= Output::grid;
  if (sotextpit > 0) {
    bool white= false;
    for (i=0; i<3; i++) {
      double intpartdummy;
      double remainder= modf(mean[i] / sotextpit, &intpartdummy);
      if (remainder >= 0.5 || (remainder >= -0.5 && remainder < 0.0))
	white= !white;
    }
    colour= white ? Output::solidwhite : Output::solidblack;
  }
  o.drawcell(array,totalnn,colour);
  delete[] array;
}                          

SortedCellList::Fragment* SortedCellList::insertfragment(int at) {
  if (used==size) {
    size+=5; size<<=1;
    fragments= (Fragment**)realloc(fragments,sizeof(Fragment*)*size);
    if (!fragments) { perror("realloc"); exit(1); }
  }
  memmove(fragments+at+1, fragments+at, sizeof(Fragment*)*(used-at));
  used++;
  Fragment *nf= new Fragment;
  fragments[at]= nf;
  return nf;
}

void SortedCellList::insert(Cell *it) {
  double index= it->index();
  int fragment;
  for (fragment= used-1;
       fragment >= 0 && index < fragments[fragment]->entries[0].index;
       fragment--);
  if (fragment < 0) {
    Fragment *nf= insertfragment(0);
    nf->used= 1;
    nf->entries[0].index= index;
    nf->entries[0].cell= it;
    return;
  }
  int entry;
  for (entry= 0;
       entry < fragments[fragment]->used &&
       index > fragments[fragment]->entries[entry].index;
       entry++);
  if (fragments[fragment]->used >= fragmentsize) {
    Fragment *nf= insertfragment(fragment+1);
    nf->used= fragmentsize>>1;
    fragments[fragment]->used -= nf->used;
    memcpy(nf->entries,
           fragments[fragment]->entries + fragments[fragment]->used,
           nf->used*sizeof(Entry));
    if (entry >= fragments[fragment]->used) {
      entry-= fragments[fragment]->used;
      fragment++;
    }
  }
  memmove(fragments[fragment]->entries + entry+1,
          fragments[fragment]->entries + entry,
          sizeof(Entry) * (fragments[fragment]->used - entry));
  fragments[fragment]->entries[entry].index= index;
  fragments[fragment]->entries[entry].cell= it;
  fragments[fragment]->used++;
}

SortedCellList::~SortedCellList() {
  for (int fragment=0; fragment<used; fragment++) {
    for (int entry=0; entry<fragments[fragment]->used; entry++) {
      delete fragments[fragment]->entries[entry].cell;
    }
    delete fragments[fragment];
  }
  free(fragments);
}

void SortedCellList::display(Output& o) {
  o.startimage();
  for (int fragment=0; fragment<used; fragment++) {
    for (int entry=0; entry<fragments[fragment]->used; entry++) {
      if (Point::indexvisible(fragments[fragment]->entries[entry].index)) {
        fragments[fragment]->entries[entry].cell->display(o);
      }
    }
  }
  o.endimage();
}
