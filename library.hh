/*
 * Points library
 */

#ifndef POINTS_HH
#define POINTS_HH

#include <stdlib.h>

/*
 * Coordinate system:
 *
 *          Y |
 *            |
 *            |
 *            /-------- X
 *           /
 *          /
 *       Z (coming out of the monitor)
 */

struct Onscreen {
  double x, y;
  Onscreen(){};
  Onscreen(double ix, double iy) { x=ix; y=iy; }
};

class Point;

class Transform {
public:
  virtual Point operator()(Point)= 0;
};

class TransformList {
  Transform **a;
  int size,used;
  void clearcontents();
 public:
  TransformList() { size=used=0; a=0; }
  ~TransformList() { clearcontents(); free(a); }
  void reset() { clearcontents(); used=0; }
  Point operator()(Point);
  void append(Transform*);
};

class Point {
  static TransformList usertransforms;
  static TransformList povtransform;
  static double planedistance, eyedistance, cutoffdistance, eyex;
  double xyz[3];
public:
  Point(){}
  Point(double x, double y, double z) { xyz[0]=x; xyz[1]=y; xyz[2]=z; }
  Point operator+(Point r) { return Point(xyz[0]+r[0], xyz[1]+r[1], xyz[2]+r[2]); }
  Point operator*(double f) { return Point(xyz[0]*f, xyz[1]*f, xyz[2]*f); }
  double& operator[](int i) { return xyz[i]; }

  operator Onscreen() const;
  Point halftransformed() const { return usertransforms(*this); }
  Point transformed() const { return povtransform(usertransforms(*this)); }
  double index() const { return transformed()[2]; }

  static int indexvisible(double index) { return index < cutoffdistance; }
  static void appendtransform(Transform *t) { usertransforms.append(t); }
  static void cleartransform() { usertransforms.reset(); }
  static void setobserver(double theta, double eta=0.0,
                          double planedist=1.0, double eyedist=1.0,
                          double cutoff=10.0);
  static void seteyex(double n) { eyex=n; }
};

#endif
