/*
 * Transformations
 */

#ifndef TRANSFORMS_HH
#define TRANSFORMS_HH

#include "library.hh"

class RotationTransform: public Transform {
protected:
  double s,c;
public:
  RotationTransform(double theta);
};

class XZRotationTransform : public RotationTransform {
  /*
   *          Y |
   *            |    <\ (positive theta)
   *            |     /
   *            /-------- X
   *           /
   *        Z / \--->
   */
public:
  XZRotationTransform(double theta) : RotationTransform(theta) { }
  Point operator()(Point);
};

class YZRotationTransform : public RotationTransform {
  /*
   *          Y | _
   *            |/ \ (positive theta)
   *          ^ |  V
   *          | /-------- X
   *          \/
   *        Z /
   */
public:
  YZRotationTransform(double theta) : RotationTransform(theta) { }
  Point operator()(Point);
};

class TearOpenTransform : public Transform {
  double factor;
  double apexy;
public:
  TearOpenTransform(double amt, double apy=1.3);
  Point operator()(Point in);
};

class BulgeTransform : public Transform {
  double amount;
  double variance;
  double apexy;
public:
  BulgeTransform(double amt, double stddev, double apy=1.1);
  Point operator()(Point in);
};

class TwistTransform : public Transform {
  double factor;
  double apexy;
public:
  TwistTransform(double amt, double apy=1.2) { factor=amt; apexy=apy; }
  Point operator()(Point in);
};

class ShearTransform : public Transform {
  Point epicentre;
  Point magnitude;
  Point deviation;
public:
  ShearTransform(Point ec, Point mag, Point dev) {
    epicentre=ec; magnitude=mag; deviation=dev;
  }
  Point operator()(Point in);
};

class ScaleTransform : public Transform {
  double scale;
public:
  ScaleTransform(double s) { scale= s; }
  Point operator()(Point in);
};

class MagicTransform : public Transform {
  double xkonstant, xfactor, yoffset, yfactor, zfactor;
public:
  MagicTransform(double xk, double xf, double zf,
                 double yfudge=0.25, double yo=0.5, double yf=-0.5) {
    xkonstant=xk; xfactor=xf; yoffset=yo+yfudge; yfactor=yf; zfactor=zf;
  }
  Point operator()(Point in);
};

#endif
