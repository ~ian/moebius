/*
 * X11 functions
 */

#include "dualx11.hh"
#include "parameter.hh"

static Parameter<int> dualx11size("dualx11size", "Dual X11 window size",
				  500, 100, 10, 10000);

DualX11Output::DualX11Output() {
  display= XOpenDisplay(0);
  Colormap cmap= DefaultColormap(display,DefaultScreen(display));

  XAllocColorCells(display,cmap,
                   False, // contig
                   planemasks,
                   2, // nplanes
                   &pixelbase,
                   1); // ncolors
  XColor colours[4], *cp= colours;
  for (int i=0; i<2; i++)
    for (int j=0; j<2; j++, cp++) {
      cp->pixel= pixelbase + (i? planemasks[0] :0) + (j? planemasks[1] :0);
      cp->red= i?65535:0;  cp->green= j?/*65535*/32000:0;  cp->blue= 0;
      cp->flags= DoRed|DoGreen|DoBlue;
    }
  XStoreColors(display,cmap,colours,4);
  
  window= XCreateSimpleWindow(display,
                              DefaultRootWindow(display),
                              0,0, dualx11size,dualx11size, 0,0, pixelbase);

  XGCValues gcvalues;
  int i;
  for (i=0; i<2; i++) {
    gcvalues.plane_mask= planemasks[i];
    // First, the fabric
    gcvalues.function= GXclear;
    fabric[i]= XCreateGC(display,window,
                         GCFunction|GCPlaneMask,
                         &gcvalues);
    // Then, the mesh
    gcvalues.function= GXset;
    mesh[i]= XCreateGC(display,window,
                       GCFunction|GCPlaneMask,
                       &gcvalues);
  }
  XSelectInput(display,window,0);
  XMapWindow(display,window);
  XFlush(display);
}

void DualX11Output::startimage() {
  XClearWindow(display,window);
}

void DualX11Output::endimage() {
  XFlush(display);
}

DualX11Output::~DualX11Output() {
  XCloseDisplay(display);
}

void DualX11Output::drawcell(const Point* list, int n, Colour colour) {
  static Parameter<double>
    eyeseparation("eyesep", "Distance from projection eye to origin", 
		  0.3, .1, 0., 100.);

  for (int i=0; i<2; i++) {
    GC fill;
    bool draw;
    
    Point::seteyex(eyeseparation*(i-0.5));

    switch (colour) {
    case grid:       fill= fabric[i]; draw= true;  break;
    case solidblack: fill= fabric[i]; draw= false; break;
    case solidwhite: fill= mesh[i];   draw= false; break;
    default: abort();
    }

    XPoint xp[n+1];
    for (int j=0; j<n; j++) {
      Onscreen here= Onscreen(list[j]);
      xp[j].x= (int)((here.x+1.0)*(dualx11size*0.5));
      xp[j].y= (int)((-here.y+1.0)*(dualx11size*0.5));
    }

    XFillPolygon(display,window,fill,xp,n,Nonconvex,CoordModeOrigin);
    if (draw) {
      xp[n]= xp[0];
      XDrawLines(display,window,mesh[i],xp,n+1,CoordModeOrigin);
    }
  }
  Point::seteyex(0);
}
