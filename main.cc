/*
 * Moebius main program.
 */

#include <math.h>
#include <iostream.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "output.hh"
#include "x11.hh"
#include "dualx11.hh"
#include "postscript.hh"
#include "library.hh"
#include "moebius.hh"
#include "transforms.hh"
#include "graphics.hh"
#include "parameter.hh"

static Parameter<int>
tn("tn", "Number of sections around `strip'", 30, 1, 1, 500);

static Parameter<int>
un("un", "Number of sections across `strip'", 15, 1, 1, 500);

static Parameter<int>
version("version", "Version to use: 0=original strip,"
	" 1=reformulated, 2=reformulated again", 0, 1, 0, 2);

static Parameter<double>
theta("theta", "Angle of model rotation",
      /*-PI/2.*/0, M_PI/8., -M_PI*2., M_PI*2.);

static Parameter<double>
eta("eta", "Angle of model elevation",
    0., M_PI/8., -M_PI*2., M_PI*2.);

static Parameter<double>
scale("scale", "Scale factor for resulting object", 1.0, 0.25, 0.001, 1000.);

static Parameter<double>
planedistance("plane", "Distance from projection plane to origin", 
	      4., .25, 0., 100.);

static Parameter<double>
eyedistance("eye", "Distance from projection eye to origin", 
	    1.5, .25, 0., 100.);

static Parameter<double>
cutoffdistance("cutoff", "Distance from projection cutoff to origin", 
	       10., .25, -10., 100.);

static Parameter<double>
width("width", "Width of the `strip' before transformation", .4, .1, 0., 1.);

static Parameter<double>
thickness("thick", "Thickness of the `pancake'", 1., .1, 0., 1.);

static Parameter<double>
bottomportion("bp", "Proportion in the bottom half", .7, .1, 0., 1.);

static Parameter<double>
tear("tear", "Angle factor in tear-open", 0/*1.1*/, .1, -5., 5.);

static Parameter<double>
teary("teary", "Axis y coord in tear-open", 1.3, .1, -10., 10.);

static Parameter<double>
twist("twist", "Angle per length in twist", 0/*.8*/, .25, -5., 5.);

static Parameter<double>
twisty("twisty", "Axis y coord in twist", .85, .1, -10., 10.);

static Parameter<double>
bulge("bulge", "Amount of bulge", 0/*1.*/, .25, -15., 15.);

static Parameter<double>
bulgesd("bulgesd", "S.D. of bulge", 1., .2, .000001, 20.);

static Parameter<double>
bulgey("bulgey", "Axis y coord in bulge", .85, .1, -10., 10.);

static Parameter<double>
shearx("shearx", "Amount of shear (x)", 0., .1, -15., 15.);

static Parameter<double>
sheary("sheary", "Amount of shear (y)", 0/*1.*/, .1, -15., 15.);

static Parameter<double>
shearsdx("shearsdx", "S.D. of shear (x)", .5, .1, -15., 15.);

static Parameter<double>
shearsdy("shearsdy", "S.D. of shear (y)", .5, .1, -15., 15.);

static Parameter<double>
shearey("shearey", "Centre of shear (y)", 1.3, .1, -10., 10.);


SortedCellList *list=0;
X11Output *x11= 0;
DualX11Output *dualx11= 0;

static Moebius *strip= 0;

void generate() {
  if (list) delete list;
  if (strip) { delete strip; strip= 0; }
  switch (version) {
  case 0: strip= new MoebiusStrip(width);                           break;
  case 1: strip= new MoebiusEnfoldment(thickness,bottomportion);    break;
  case 2: strip= new MoebiusEnfoldmentNew();                        break;
  default: abort();
  }
  list= new SortedCellList;
  double t0= 0.0;
  for (int ti=0; ti<tn; ti++) {
    double t1= (double)(ti+1)/tn;
    double u0= 0.0;
    for (int ui=0; ui<un; ui++) {
      double u1= (double)(ui+1)/un;
      Cell *cell= new Cell;
//u0=u1=tear;
      cell->p[0]= strip->middlepoint(t0,u0);
//fprintf(stderr,"t %7.4f u %7.4f  %7.4f,%7.4f,%7.4f\n",
//        t0,u0, cell->p[0][0], cell->p[0][1], cell->p[0][2]);
      cell->p[1]= strip->middlepoint(t0,u1);
      cell->p[2]= strip->middlepoint(t1,u1);
      cell->p[3]= strip->middlepoint(t1,u0);
      list->insert(cell);
      u0= u1;
    }
    t0= t1;
  }
}

int main(int argc, char **argv) {
  static const char pointmap_cmd[]= "pointmap ";
  char buf[100];
  double pointmap_t, pointmap_u;

  for (;;) {
    Point::setobserver(theta,eta,planedistance,eyedistance,cutoffdistance);
    Point::cleartransform();
    if (tear != 0.0) Point::appendtransform(new TearOpenTransform(tear,teary));
    if (twist != 0.0) Point::appendtransform(new TwistTransform(twist,twisty));
    if (bulge != 0.0) Point::appendtransform(new BulgeTransform(bulge,bulgesd,bulgey));
    if (shearx != 0.0 || sheary != 0.0)
      Point::appendtransform(new ShearTransform(Point(0.0,shearey,0.0),
                                                Point(shearx,sheary,0.0),
                                                Point(shearsdx,shearsdy,1e20)));
    Point::appendtransform(new ScaleTransform(scale));
    generate();
    if (x11) list->display(*x11);
    if (dualx11) list->display(*dualx11);
    for (;;) {
      cerr << "> ";
      cin.getline(buf,sizeof(buf),'\n');
      if (!cin.good()) {
	cerr << "error reading command input, giving up\n";
        exit(1);
      }
      char *equals= strchr(buf,'=');
      if (equals) {
        *equals++= 0;
        AnyParameter *param= AnyParameter::find(buf);
        if (param) {
          *param= atof(equals);
          break;
        } else {
          cerr << "parameter `" << buf << "' not found\n";
        }
      } else if (!strncmp(buf,"postscript ",11)) {
        const char *filename= buf+11;
        ofstream file(filename);
        if (file) {
          PostScriptOutput psout(file);
          list->display(psout);
        } else {
          cerr << "Failed to open `" << filename << "'\n";
          continue;
        }
        cerr << "PostScript written to `" << filename <<"'\n";
      } else if (!strcmp(buf,"x11")) {
        if (x11) {
          delete x11;
          x11= 0;
        } else {
          x11= new X11Output;
        }
        break;
      } else if (!strcmp(buf,"dualx11")) {
        if (dualx11) {
          delete dualx11;
          dualx11= 0;
        } else {
          dualx11= new DualX11Output;
        }
        break;
      } else if (!strcmp(buf,"quit")) {
        exit(0);
      } else if (!strcmp(buf,"list")) {
        AnyParameter::help();
      } else if (!strcmp(buf,"help")) {
        AnyParameter::list();
      } else if (!strncmp(buf,pointmap_cmd,sizeof(pointmap_cmd)-1)) {
	if (sscanf(buf+sizeof(pointmap_cmd)-1, "%lf %lf",
		   &pointmap_t, &pointmap_u) != 2) {
	  cerr << "failed to parse pointmap args\n";
	  continue;
	}
	Point p= strip->middlepoint(pointmap_t,pointmap_u);
	cout << "\n";
	for (int i=0; i<3; i++) cout << p.halftransformed()[i] << "\n";
      } else if (!*buf) {
        break;
      } else {
        cerr << "command not understood\n";
      }
    }
  }
}
