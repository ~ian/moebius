/*
 * Parameters library
 */

#ifndef PARAMETER_HH
#define PARAMETER_HH

#include <iostream.h>

class AnyParameter {
  AnyParameter *next;
  static AnyParameter *first;
protected:
  const char *name;
  const char *description;
  void printvalue();
  virtual void rangecheckprint() =0;
public:
  AnyParameter(const char *n, const char *d);
  virtual void operator ++() =0;
  virtual void operator --() =0;
  virtual void operator =(double) =0;
  static AnyParameter *find(const char *n);
  static void list();
  static void help();
};

template<class T> class Parameter : AnyParameter {
  T value, delta, min, max;
  void rangecheckprint();
public:
  Parameter(const char *n, const char *d, T i, T de, T mi, T ma);
  operator T (){ return value; }
  void operator ++(){ value+= delta; rangecheckprint(); }
  void operator --(){ value-= delta; rangecheckprint(); }
  void operator =(double v) { value= (T)v; rangecheckprint(); }
};

template<class T> Parameter<T>::Parameter
    (const char *n, const char *d, T i, T de, T mi, T ma)
: AnyParameter(n,d) {
  value=i; delta=de; min=mi; max=ma;
}

template<class T> void Parameter<T>::rangecheckprint() {
  if (value<min) {
    value= min;  cerr << "underflowed; ";
  } else if (value>max) {
    value= max;  cerr << "overflowed; ";
  }
  cerr << "set to " << value << "\n";
}

#endif
