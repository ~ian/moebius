/*
 * Parameters library
 */

#include "parameter.hh"

AnyParameter::AnyParameter(const char *n, const char *d) {
  name= n; description= d;
  next= first;
  first= this;
}

AnyParameter* AnyParameter::find(const char *n) {
  AnyParameter* search;
  for (search= first;
       search && strcmp(search->name,n);
       search= search->next);
  return search;
}

void AnyParameter::printvalue(void) {
  cerr << name << " ";
  rangecheckprint();
}

void AnyParameter::list() {
  for (AnyParameter* search= first;
       search;
       search= search->next) {
    cerr << search->description << ": ";
    search->printvalue();
  }
}

void AnyParameter::help() {
  for (AnyParameter* search= first;
       search;
       search= search->next) {
    search->printvalue();
  }
}

AnyParameter* AnyParameter::first= 0;
