/*
 * Graphics library
 */

#ifndef GRAPHICS_HH
#define GRAPHICS_HH

#include "library.hh"
#include "output.hh"

class Cell {
  void display(Output& o);
  friend class SortedCellList;
public:
  Point p[4];
  double index() { return p[0].index(); }
};

#define fragmentsize 10
class SortedCellList {
  struct Entry {
    double index;
    Cell *cell;
  };
  struct Fragment {
    int used;
    Entry entries[fragmentsize];
  };
  Fragment **fragments;
  int size,used;
  
  Fragment *insertfragment(int at);
public:
  SortedCellList() { fragments=0; size=used=0; }
  ~SortedCellList();
  void insert(Cell*);
  void display(Output& o);
};

#endif
