/*
 * Equation for a Moebius strip
 */

#include <math.h>
#include <stdio.h>

#include "library.hh"
#include "moebius.hh"

Point MoebiusStrip::edgepoint(double t) {
  double theta= (t-0.5)*4.0*M_PI;
  double r= 1.0 - halfgap*(1.0 - sin(theta/2.0));
  return Point(r*sin(theta),
               -r*cos(theta),
               halfbreadth*cos(theta/2.0));
}

Point MoebiusStrip::middlepoint(double t, double u) {
  return edgepoint(t*0.5)*u + edgepoint((t+1)*0.5)*(1.0-u);
}

class Bezier {
  double e,f,g,h;
public:
  Bezier(double x0, double x1, double dx0, double dx1);
  double operator()(double t) { return h + t*(g + t*(f + t*e)); }
  void debug();
};

Bezier::Bezier(double x0, double x1, double dx0, double dx1) {
//  e= 2*x0 - 2*x1 + dx0 + dx1;
//  f= x1 - dx0 - x0 - e;
  h= x0;
  g= dx0;
  e= g + 2*h + dx1 - 2*x1;
  f= x1 - e - g - h;
}

void Bezier::debug() {
  fprintf(stderr,"bz e %7.4f f %7.4f g %7.4f h %7.4f\n",e,f,g,h);
}

// The first end is at [sin(theta/2),-cos(theta/2),0]
// The second end is at [-theta/pi,0,sin(theta)]
// The first end is oriented towards [0,cos(theta),sin(theta)]
// The second end is oriented towards [0,-1,0]

Point MoebiusEnfoldmentAny::edgepoint(double t) {
  double theta= t*2.0*M_PI;
  return Point(sin(theta),cos(theta),0);
}

Point MoebiusEnfoldment::middlepoint(double t, double u) {
  if (t > bottomportion) {
    t -= bottomportion;
    t /= (1.0 - bottomportion);
    double sizehere= sqrt(1-t*t);
    return Point((u*2.0-1.0) * sizehere,
                 t,
                 sizehere * thickness * sin(u*2.0*M_PI));
  } else {
    t /= bottomportion;
    double theta= (.5-u)*2*M_PI;
    Bezier bx(sin(theta*.5), -theta/M_PI, 0, 0);
    double ypushiness= (1-cos(theta))*2.5+1;
//        double fudge= (M_PI*sin(theta*.5)*cos(theta*.5))*(.5-u)*(.5-u)*4;
    double fudge= (.5-u)*(.5-u)*4*cos(theta*.5);
    Bezier by(-cos(theta*.5), 0,
              cos(theta)*ypushiness + fudge*ypushiness,
              ypushiness);
//by.debug();
    Bezier bz(0, sin(theta), sin(theta), 0);
    return Point( bx(t), by(t), thickness * bz(t) );
  }
}    

Point MoebiusEnfoldmentNew::middlepoint(double t, double u) {
  if (u <= 0.5) {
    if (t <= 0.5) {
      double sin_pi_t= sin(M_PI*t);
      double cos_pi_t= cos(M_PI*t);
      double sin_2pi_t= sin(2.0*M_PI*t);
      double cos_2pi_t= cos(2.0*M_PI*t);

      double prop_circ= 0.5+0.5*cos(2.0*M_PI*u);
      //      double prop_circ= 1.0-2.0*u;

      Point p_edge(-sin_pi_t,cos_pi_t,0);

      double alpha_circ= (2.0*M_PI)*(2.0*t)*(2.0*u);
      Point v_edge1(cos_2pi_t*sin_pi_t,-cos_2pi_t*cos_pi_t,sin_2pi_t);
      Point v_edge2(sin_2pi_t*cos_pi_t,-sin_2pi_t*sin_pi_t,-cos_2pi_t);
      Point p_circ_cent= p_edge + v_edge2*(2.0*t);
      Point p_circ_pt= p_circ_cent + (v_edge1*sin(alpha_circ) + v_edge2*-cos(alpha_circ))*(2.0*t);
      p_circ_pt[1]= p_edge[1];

      Point v_line(-cos_pi_t,0,sin_pi_t);
      Point p_line_start(0,1.0-2.0*t,0);
      Point p_line_pt= p_line_start + v_line*(1.0-2.0*u)*(M_PI*t);

      return p_circ_pt*prop_circ + p_line_pt*(1.0-prop_circ);
    } else {
      return middlepoint(0.5,u) + Point(0,0.5-t,0);
    }
  } else {
    Point p_mirror= middlepoint(t,1.0-u);
    return Point(-p_mirror[0], p_mirror[1], -p_mirror[2]);
  }
}
