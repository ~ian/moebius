/*
 * PostScript output
 */

#include <fstream.h>

#include "output.hh"

struct PostScriptOutput : Output {
  PostScriptOutput(ofstream&);
  ~PostScriptOutput();
  void drawcell(const Point *list, int n, Colour colour);
private:
  ofstream& file;
  void docell(const Onscreen*, int, const char* what);
  void preamble();
};
